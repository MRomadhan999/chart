class CreateSiswas < ActiveRecord::Migration[5.0]
  def change
    create_table :siswas do |t|
      t.string :nama
      t.string :kelas
      t.integer :skor_uts
      t.integer :skor_uas
      t.float :rata_rata

      t.timestamps
    end
  end
end
