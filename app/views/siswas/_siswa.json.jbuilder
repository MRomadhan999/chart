json.extract! siswa, :id, :nama, :kelas, :skor_uts, :skor_uas, :rata_rata, :created_at, :updated_at
json.url siswa_url(siswa, format: :json)
